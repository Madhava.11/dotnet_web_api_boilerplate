﻿using Contracts;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerService
{
    public  class LoggerManager:ILoggerManager
    {
        private static ILogger logger = LogManager.GetCurrentClassLogger();
        public void LogDebug(string controllerName, string methodName, string message) => logger.Debug($"Class: {controllerName}|Method:{methodName}| {message}");
        public void LogError(string controllerName, string methodName, string message) => logger.Error($"Class: {controllerName}|Method:{methodName}|{message}");
        public void LogInfo(string controllerName, string methodName,string message) => logger.Info($"Class: {controllerName}|Method:{methodName}|{message}");
        public void LogWarn(string controllerName, string methodName, string message) => logger.Warn($"Class: {controllerName}|Method:{methodName}|{message}");
    }
}
