﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public interface ILoggerManager
    {
        void LogInfo(string controllerName, string methodNmae,string message);
        void LogWarn(string controllerName, string methodNmae, string message);
        void LogDebug(string controllerName, string methodNmae, string message);
        void LogError(string controllerName, string methodNmae, string message);
    }
}
