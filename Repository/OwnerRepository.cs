﻿using Contracts;
using Entities.Models;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Entities.Helper;
using Microsoft.Extensions.Caching.Memory;

namespace Repository
{
    public class OwnerRepository : RepositoryBase<Owner>, IOwnerRepository
    {
        public OwnerRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }

        public async Task<PagedList<Owner>> GetAllOwnersAsync(OwnerParameters ownerParameters)
        {
            /* return await FindAll()
                 .OrderBy(ow => ow.Name)
                 .Skip((ownerParameters.PageNumber - 1) * ownerParameters.PageSize)
                 .Take(ownerParameters.PageSize)
                 .ToListAsync();*/
           
                var res = await PagedList<Owner>.ToPagedList(FindAll().OrderBy(on => on.Name).AsNoTracking(),
                     ownerParameters.PageNumber,
                     ownerParameters.PageSize);
            return res;
        }

        public async Task<Owner> GetOwnerByIdAsync(Guid ownerId)
        {
            return await  FindByCondition(owner => owner.Id.Equals(ownerId)).AsNoTracking()
            .FirstOrDefaultAsync();
        }

        public async Task<Owner> GetOwnerWithDetailsAsync(Guid ownerId)
        {
            return await  FindByCondition(owner => owner.Id.Equals(ownerId))
                .Include(ac => ac.Accounts).AsNoTracking()
                .FirstOrDefaultAsync();
        }

        public void CreateOwner(Owner owner)
        {
            Create(owner);
        }
        public void UpdateOwner(Owner owner)
        {
            Update(owner);
        }

        public void DeleteOwner(Owner owner)
        {
            Delete(owner);
        }
    }
}
