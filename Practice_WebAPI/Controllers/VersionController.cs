﻿using Asp.Versioning;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Practice_WebAPI.Controllers
{
  
    [ApiController]
    [ApiVersion(1, Deprecated =true)]
    [ApiVersion(2)]
    [Route("api/v{v:apiVersion}/[controller]")]
    public class VersionController : ControllerBase
    {
        public static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
         };

        [MapToApiVersion(1)]
        [HttpGet]
        [Route("Summaries")]
        public IEnumerable<string> Get() 
        {
            return Summaries.Where(x => x.StartsWith("B"));
        }

        [MapToApiVersion(2)]
        [HttpGet]
        [Route("Summaries")]
        public IEnumerable<string> Getv2()
        {
            return Summaries.Where(x => x.StartsWith("C"));
        }
    }
}
