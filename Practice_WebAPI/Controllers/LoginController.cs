﻿using AutoMapper;
using Contracts;
using Entities.Models;
using Entities.Models.Email;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace Practice_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {

        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        private readonly ILoggerManager _logger;

        public LoginController(IMapper mapper, 
            UserManager<User> userManager,
            IConfiguration configuration,
            ILoggerManager logger,
            SignInManager<User> signInManager,
            IEmailService emailService)
        {
            _mapper = mapper;
            _userManager = userManager;
            _configuration = configuration;
            _logger = logger;
            _signInManager = signInManager;
            _emailService = emailService;
        }

        [HttpPost]
        public async Task<IActionResult> Login(UserLogin userModel)
        {
            var user = await _userManager.FindByEmailAsync(userModel.Email);
            if (user != null &&
                await _userManager.CheckPasswordAsync(user, userModel.Password))
            {
                    await _signInManager.SignOutAsync();
                    await _signInManager.PasswordSignInAsync(user, userModel.Password, false, true);
                    var token = await _userManager.GenerateTwoFactorTokenAsync(user, "Email");

                    var message = new Message(new string[] { user.Email! }, "OTP Confirmation", token);
                    _emailService.SendEmail(message);

                    return Ok($"We have sent an OTP to your Email {user.Email}");             
            }
            else
            {
                _logger.LogError(nameof(LoginController), nameof(Login), $"Invalid Login: User {userModel.Email}");
                return BadRequest("Invalid UserName or Password");
            }
        }

        [HttpPost]
        [Route("login-2FA")]
        public async Task<IActionResult> LoginWithOTP(string code, string username)
        {
            var user = await _userManager.FindByEmailAsync(username);
            var signIn = await _signInManager.TwoFactorSignInAsync("Email", code, false, false);
            if (signIn.Succeeded && user!=null)
            {
                    var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };
                    var userRoles = await _userManager.GetRolesAsync(user);
                    foreach (var role in userRoles)
                    {
                        authClaims.Add(new Claim(ClaimTypes.Role, role));
                    }

                    var jwtToken = GetToken(authClaims);
                    var refreshTokenGenerated = GenerateRefreshToken();
                    user.RefreshToken= refreshTokenGenerated;
                    user.RefreshTokenExpiry = DateTime.UtcNow.AddDays(3);
                    await _userManager.UpdateAsync(user);
                    return Ok(new
                    {
                        token = new JwtSecurityTokenHandler().WriteToken(jwtToken),
                        tokenExpiry = jwtToken.ValidTo,
                        refreshToken = refreshTokenGenerated,
                        refreshTokenExpiry=user.RefreshTokenExpiry
                        
                    });
            }
            return BadRequest("Invalid Code");
        }

        [HttpPost]
        [Route("forgotPassword")]
        public async Task<IActionResult> ForgotPassword([Required]string email) 
        {
            var user=await _userManager.FindByEmailAsync(email);
            if (user != null)
            {
                 var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var forgotPasswordLink=Url.Action(nameof(ResetPassword),"Login", new {token, email}, Request.Scheme);
                var message = new Message(new string[] { user.Email! }, "Forgot Password", forgotPasswordLink);
                _emailService.SendEmail(message);
                return Ok($"Password change link sent to email {email}");
            }
            else
            {
                return BadRequest($"Email not found");
            }

        }

        [HttpPost]
        [Route("resetPassword")]
        public async Task<IActionResult> ResetPassword(ResetPassword resetPassword)
        {
            var user = await _userManager.FindByEmailAsync(resetPassword.Email);
            if (user != null)
            {
                var resetPassResult=await _userManager.ResetPasswordAsync(user, resetPassword.Token,resetPassword.Password);
                if(!resetPassResult.Succeeded)
                {
                    foreach (var error in resetPassResult.Errors)
                    {
                        ModelState.AddModelError(error.Code, error.Description);
                    }
                    return BadRequest(ModelState);
                }
                return Ok("Password Changed Successfully");
            }
            return BadRequest("Email not Found");
        }

        [HttpPost]
        [Route("Refresh-Token")]
        public async Task<IActionResult> RenewAccessTokenAsync(LoginResponse token)
        {
            var accessToken = token.AccessToken;
            var refreshToken=token.RefreshToken;
            var principal= GetClaimsPrincipal(accessToken);
            var user = await _userManager.FindByNameAsync(principal.Identity.Name);
            if(refreshToken!= user.RefreshToken)
            {
                return BadRequest("Please Login");
            }

            var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };
            var userRoles = await _userManager.GetRolesAsync(user);
            foreach (var role in userRoles)
            {
                authClaims.Add(new Claim(ClaimTypes.Role, role));
            }

            var jwtToken = GetToken(authClaims);
            var rToken = GenerateRefreshToken();
            user.RefreshToken = refreshToken;
            user.RefreshTokenExpiry = DateTime.UtcNow.AddDays(3);
            await _userManager.UpdateAsync(user);
            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(jwtToken),
                tokenExpiry = jwtToken.ValidTo,
                refreshToken = rToken,
                refreshTokenExpiry = user.RefreshTokenExpiry

            });

        }

        private ClaimsPrincipal GetClaimsPrincipal(string accessToken) 
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidateIssuerSigningKey = true,
                ValidAudience = _configuration["JWT:ValidAudience"],
                ValidIssuer = _configuration["JWT:ValidIssuer"],
                IssuerSigningKey =new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"])),
                ValidateLifetime=false
            };

            var tokenHandler=new JwtSecurityTokenHandler();
            var principal=tokenHandler.ValidateToken(accessToken, tokenValidationParameters, out SecurityToken securityToken);
            return principal;
        }

        private JwtSecurityToken GetToken(List<Claim> authClaims)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

            var token = new JwtSecurityToken(
                issuer: _configuration["JWT:ValidIssuer"],
                audience: _configuration["JWT:ValidAudience"],
                expires: DateTime.Now.AddHours(1),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );

            return token;
        }

        private string GenerateRefreshToken()
        {
            var randomNumber = new Byte[64];
            var range=RandomNumberGenerator.Create();
            range.GetBytes(randomNumber);
            return Convert.ToBase64String(randomNumber);
        }
    }
}
