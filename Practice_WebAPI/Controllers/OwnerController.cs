﻿using Asp.Versioning;
using AutoMapper;
using Contracts;
using Entities.DataTransferObjects;
using Entities.Helper;
using Entities.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System.Text;

namespace Practice_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class OwnersController : ControllerBase
    {
        private ILoggerManager _logger;
        private IRepositoryWrapper _repository;
        private readonly IMemoryCache _cacheService;
        private readonly IDistributedCache _distributedCache; 
        private IMapper _mapper;
        public OwnersController(ILoggerManager logger, 
            IRepositoryWrapper repository, 
            IMapper mapper,
            IMemoryCache cacheService,
            IDistributedCache distributedCache)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
            _cacheService = cacheService;
            _distributedCache = distributedCache;
        }
        [HttpGet]
       // [ApiVersion("1.0")]
       // [Route("/v{v:apiVersion}/Owners")]
        public async Task<IActionResult> GetAllOwners([FromQuery] OwnerParameters ownerParameters)
        {
            try
            {
                PagedList<Owner> owners;
                if (!_cacheService.TryGetValue($"{ownerParameters.PageNumber}{ownerParameters.PageSize}", out owners))
                {
                    owners = await _repository.Owner.GetAllOwnersAsync(ownerParameters);
                    var cacheEntryOptions = new MemoryCacheEntryOptions
                    {
                        SlidingExpiration = TimeSpan.FromMinutes(1), //sliding expiration
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5),  //absolute expiration
                        Priority = CacheItemPriority.High, // entries with lower priority may be removed first to free up space
                        Size = 1024  //specifies the size of the cache entry
                    };
                    _cacheService.Set<PagedList<Owner>>($"{ownerParameters.PageNumber}{ownerParameters.PageSize}", owners, cacheEntryOptions);
                }
                    var metadata = new
                {
                    owners.TotalCount,
                    owners.PageSize,
                    owners.CurrentPage,
                    owners.TotalPages,
                    owners.HasNext,
                    owners.HasPrevious
                };
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));
                _logger.LogInfo(nameof(OwnersController),nameof(GetAllOwners),$"Returned {owners.TotalCount} owners from database.");

                var ownersResult = _mapper.Map<IEnumerable<OwnerDto>>(owners);
                return Ok(ownersResult);
            }
            catch (Exception ex)
            {
                _logger.LogError(nameof(OwnersController), nameof(GetAllOwners), $"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}", Name = "OwnerById")]
        public async Task<IActionResult> GetOwnerById(Guid id)
        {
            try
            {
                var owner =await _repository.Owner.GetOwnerByIdAsync(id);

                if (owner is null)
                {
                    _logger.LogError(nameof(OwnersController), nameof(GetOwnerById),$"Owner with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInfo(nameof(OwnersController), nameof(GetOwnerById),$"Returned owner with id: {id}");
                    var ownerResult = _mapper.Map<OwnerDto>(owner);
                    return Ok(ownerResult);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(nameof(OwnersController), nameof(GetOwnerById),$"Something went wrong inside GetOwnerById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpGet("{id}/Accounts")]
        public async Task<IActionResult> GetOwnerWithDetails(Guid id)
        {
            try
            {
                var cacheKey=id.ToString();

                string serializedOwner;
                Owner owner = new Owner();
                serializedOwner = await _distributedCache.GetStringAsync(cacheKey);
                if (serializedOwner != null)
                {
                    owner = JsonConvert.DeserializeObject<Owner>(serializedOwner);
                }
                else
                {
                    owner = await _repository.Owner.GetOwnerWithDetailsAsync(id);
                   
                    if (owner == null)
                    {
                        _logger.LogError(nameof(OwnersController), nameof(GetOwnerWithDetails), $"Owner with id: {id}, hasn't been found in db.");
                        return NotFound();
                    }
                    else
                    {
                        _logger.LogInfo(nameof(OwnersController), nameof(GetOwnerWithDetails), $"Returned owner with details for id: {id}");
                       serializedOwner=JsonConvert.SerializeObject(owner, Formatting.Indented, 
                           new JsonSerializerSettings
                           {
                               ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                           });
                        var options = new DistributedCacheEntryOptions()
                            .SetSlidingExpiration(TimeSpan.FromMinutes(5))
                            .SetAbsoluteExpiration(TimeSpan.FromHours(5));
                            
                        await _distributedCache.SetStringAsync(cacheKey, serializedOwner, options);
                    }                  
                }
                var ownerResult = _mapper.Map<OwnerDto>(owner);
                return Ok(ownerResult);
            }
            catch (Exception ex)
            {
                _logger.LogError(nameof(OwnersController), nameof(GetOwnerWithDetails), $"Something went wrong inside GetOwnerWithDetails action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateOwner([FromBody] OwnerForCreationDto owner)
        {
            try
            {
                if (owner is null)
                {
                    _logger.LogError(nameof(OwnersController), nameof(CreateOwner),"Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }

                if (!ModelState.IsValid) //returns false if any validations are failed defined in OwnerForCreationDto
                {
                    _logger.LogError(nameof(OwnersController), nameof(CreateOwner), "Invalid owner object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var ownerEntity = _mapper.Map<Owner>(owner);

                _repository.Owner.CreateOwner(ownerEntity);
                _repository.Save();

                var createdOwner = _mapper.Map<OwnerDto>(ownerEntity);

                return CreatedAtRoute("OwnerById", new { id = createdOwner.Id }, createdOwner);
                //return a status code 201, which stands for Created. it will populate the body of the response with the newly created object
                //as well as the Location attribute within the response header with the address to retrieve that owner. We need to provide the
                //name of the action, where we can retrieve the created entity
            }
            catch (Exception ex)
            {
                _logger.LogError(nameof(OwnersController), nameof(CreateOwner), $"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOwner(Guid id, [FromBody] OwnerForUpdateDto owner)
        {
            try
            {
                if (owner is null)
                {
                    _logger.LogError(nameof(OwnersController), nameof(UpdateOwner), "Owner object sent from client is null.");
                    return BadRequest("Owner object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError(nameof(OwnersController), nameof(UpdateOwner), "Invalid owner object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var ownerEntity = await _repository.Owner.GetOwnerByIdAsync(id);
                if (ownerEntity is null)
                {
                    _logger.LogError(nameof(OwnersController), nameof(UpdateOwner), $"Owner with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _mapper.Map(owner, ownerEntity);

                _repository.Owner.UpdateOwner(ownerEntity);
                await _repository.SaveAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError(nameof(OwnersController), nameof(UpdateOwner), $"Something went wrong inside UpdateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOwner(Guid id)
        {
            try
            {
                var owner = await _repository.Owner.GetOwnerByIdAsync(id);
                if (owner == null)
                {
                    _logger.LogError(nameof(OwnersController), nameof(DeleteOwner), $"Owner with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                if (_repository.Account.AccountsByOwner(id).Result.Any())
                {
                    _logger.LogError(nameof(OwnersController), nameof(DeleteOwner), $"Cannot delete owner with id: {id}. It has related accounts. Delete those accounts first");
                    return BadRequest("Cannot delete owner. It has related accounts. Delete those accounts first");
                }

                _repository.Owner.DeleteOwner(owner);
                await _repository.SaveAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError(nameof(OwnersController), nameof(DeleteOwner), $"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
