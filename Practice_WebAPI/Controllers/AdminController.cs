﻿using AutoMapper;
using Contracts;
using Entities;
using Entities.DataTransferObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata.Ecma335;

namespace Practice_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Administrator")]
    public class AdminController : ControllerBase
    {
        private readonly RepositoryContext _context;
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;
        public AdminController(RepositoryContext repositoryContext,
            IMapper mapper,
            ILoggerManager logger)
        {
            _context = repositoryContext;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet("User")]
      
        public async Task<IActionResult> Users()
        {
            try
            {
               var users= await _context.Users.ToListAsync();
                var usersdto = _mapper.Map<IEnumerable<UserDto>>(users);
                _logger.LogInfo(nameof(AdminController),nameof(Users),$"Returned Users Details. count {usersdto.Count()}");
                return Ok(usersdto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("User/{email}")]
        public async Task<IActionResult> UserByEmail(string email)
        {
            try
            {
                var user = await _context.Users.Where(u => u.UserName.Equals(email)).FirstOrDefaultAsync();
                if (user == null)
                {
                    return NotFound("User not found");
                }
                var userdto=_mapper.Map<UserDto>(user);
                return Ok(userdto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
    }
}
