﻿using AutoMapper;
using Contracts;
using Entities.Models;
using Entities.Models.Email;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Practice_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserRegistrationController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IEmailService _emailService;

        public UserRegistrationController(IMapper mapper, 
            UserManager<User> userManager, 
            RoleManager<IdentityRole> roleManager,
            IEmailService emailService)
        {
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
            _emailService = emailService;
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] UserRegistration userModel)
        {
            /* if (!ModelState.IsValid)  // no need for web api's since we use [ApiController] attribute it will automatically do the modelstate validation and throw error.
             {
                 return BadRequest("Invalid Registration Details");
             }*/

            var userExist = await _userManager.FindByEmailAsync(userModel.Email);
            if (userExist != null)
            {
                return Forbid("User already exists!");
            }

            if (await _roleManager.RoleExistsAsync(userModel.Role))
            {
                var user = _mapper.Map<User>(userModel);
                user.TwoFactorEnabled = true;
                var result = await _userManager.CreateAsync(user, userModel.Password);
                if (!result.Succeeded)
                {
                    /*foreach (var error in result.Errors)
                    {
                        ModelState.TryAddModelError(error.Code, error.Description);
                    }*/
                    return BadRequest("Invalid Registration Details");
                }
                await _userManager.AddToRoleAsync(user, userModel.Role);

                var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                var confirmationLink = Url.Action(nameof(ConfirmEmail), "UserRegistration", new { token, email = user.Email }, Request.Scheme);
                var message = new Message(new string[] { user.Email! }, "Confirmation email link", confirmationLink);
                _emailService.SendEmail(message);

                return Ok("Registered Successfully");
            }
            else
            {
                return BadRequest("Role Doesn't Exist");
            }
        }

        [HttpGet("ConfirmEmail")]
        public async Task<IActionResult> ConfirmEmail(string token, string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user != null)
            {
                var result = await _userManager.ConfirmEmailAsync(user, token);
                if (result.Succeeded)
                {
                    return Ok("Email Verified Successfully" );
                }
            }
            return BadRequest("This User Doesnot exist!");
        }
    }
}
