﻿using AutoMapper;
using Entities.DataTransferObjects;
using Entities.Models;

namespace Practice_WebAPI
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            CreateMap<Owner, OwnerDto>();

            CreateMap<Account, AccountDto>();

            CreateMap<OwnerForCreationDto, Owner>();
            CreateMap<OwnerForUpdateDto, Owner>();

            CreateMap<UserRegistration, User>()
           .ForMember(u => u.UserName, opt => opt.MapFrom(x => x.Email));

            CreateMap<User, UserDto>();
        }
    }
}
