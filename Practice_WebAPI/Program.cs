using Asp.Versioning;
using Contracts;
using Entities;
using Entities.Models;
using Entities.Models.Email;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NLog;
using Practice_WebAPI.Extensions;
using Repository;
using System.Text;
using System.Text.Json.Serialization;

namespace Practice_WebAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.ConfigureCors();

            builder.Services.ConfigureIISIntegration();

            LogManager.Setup().LoadConfigurationFromFile();

            builder.Services.ConfigureLoggerService();

            builder.Services.ConfigureSqlContext(builder.Configuration);

            builder.Services.AddIdentity<User, IdentityRole>(opt =>
            {
                opt.Password.RequiredLength = 7;
                opt.User.RequireUniqueEmail = true;
            })
            .AddEntityFrameworkStores<RepositoryContext>()
            .AddDefaultTokenProviders();

            builder.Services.Configure<IdentityOptions>(
                opt => opt.SignIn.RequireConfirmedEmail = true
                );

            builder.Services.Configure<DataProtectionTokenProviderOptions>(opts => opts.TokenLifespan = TimeSpan.FromMinutes(60));

            builder.Services.ConfigureAuthentication(builder.Configuration);
            
            builder.Services.AddSingleton(builder.Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>());

           

            builder.Services.ConfigureServices();

            builder.Services.AddAutoMapper(typeof(Program));

            builder.Services.AddMemoryCache(); //added to use in-memory cache

            builder.Services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = "localhost:6379";
            });                   //added to use redis cache


           builder.Services.AddControllers().AddJsonOptions(x=>x.JsonSerializerOptions.ReferenceHandler=ReferenceHandler.IgnoreCycles);
            builder.Services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;//Reports the supported API versions in the api-supported-versions response header.
                options.AssumeDefaultVersionWhenUnspecified = true;//Uses the DefaultApiVersion when the client didn't provide an explicit version.
                options.DefaultApiVersion = new ApiVersion(1, 0);// Sets the default API version. Typically, this will be v1.0.
                options.ApiVersionReader = ApiVersionReader.Combine(//Configures how to read the API version specified by the client. The default value is QueryStringApiVersionReader.
                    new QueryStringApiVersionReader("api-version"),
                    new HeaderApiVersionReader("X-Api-Version"),
                    new MediaTypeApiVersionReader("ver"));
            }).AddApiExplorer(options =>//for swagger
            {
                options.GroupNameFormat = "'v'V";
                options.SubstituteApiVersionInUrl = true;
            });
            builder.Services.AddEndpointsApiExplorer();

            builder.Services.ConfigureSwagger();
            

            var app = builder.Build();
           
            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();

            app.UseStaticFiles(); //enables using static files for the request. If we don�t set a path to the static files, it will use a wwwroot folder in our solution explorer by default.

            var logger = app.Services.GetRequiredService<ILoggerManager>();
            app.ConfigureExceptionHandler(logger);

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.All
            }); // will forward proxy headers to the current request. This will help us during the Linux deployment.

            app.UseCors("CorsPolicy");

            app.UseAuthentication();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}
